#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 10 19:12:52 2020

@author: ele
"""

import json
import pickle
import glob
import shutil
import unittest
from ieaiaio import IOUtils as iou

class TestIOUtils(unittest.TestCase):
    
    cwd = glob.os.getcwd()
    sub = 'test_folder'
    file = 'test_file'
    test_path = glob.os.path.join(cwd,sub)
    test_dict = {"this" : 1, "is" : 2, "a" : 3, "test": 4, "!" : 5}
    txt_file = glob.os.path.join(test_path,'.'.join([file,'txt']))
    json_file = glob.os.path.join(test_path,'.'.join([file,'json']))
    pkl_file = glob.os.path.join(test_path,'.'.join([file,'pkl']))
    
    @classmethod
    def setUpClass(cls):
        
        if not glob.os.path.isdir(cls.test_path):
            glob.os.makedirs(cls.test_path)
        
        with open(cls.txt_file,'w') as out:
            for k in cls.test_dict.keys():
                out.write("{}\n".format(k))
        
        with open(cls.json_file,'w') as out:
            json.dump(cls.test_dict, out)
        
        with open(cls.pkl_file,'wb') as out:
            pickle.dump(cls.test_dict,out)
            
    @classmethod
    def tearDownClass(cls):
        
        shutil.rmtree(cls.test_path)
                
    def test_mkdir(self):
        
        iou.mkdir(self.test_path)
        
        self.assertTrue(glob.os.path.isdir(self.test_path))
    
    def test_rm(self):
        
        sub_test_path = glob.os.path.join(self.test_path,'subfolder')
        glob.os.makedirs(sub_test_path)
        
        sub_test_file = glob.os.path.join(sub_test_path,'subfile')
        
        with open(sub_test_file,'w') as out:
            out.write("test")
        
        iou.rm(sub_test_file)
        self.assertTrue(not glob.os.path.isfile(sub_test_file))
        
        iou.rm(sub_test_path)
        self.assertTrue(not glob.os.path.isdir(sub_test_path))
        
        
    def test_exists(self):
        
        self.assertTrue(iou.exists(self.cwd))
        
    def test_join_paths(self):
        
        out = iou.join_paths([self.cwd,self.file])
        self.assertEqual(out,glob.os.path.join(self.cwd,self.file))
        
    def test_dname(self):
        
        out = iou.dname(iou.join_paths([self.cwd,self.sub]))
        self.assertEqual(out,self.cwd)
    
    def test_fname(self):
        
        out = iou.fname(iou.join_paths([self.cwd,self.file]))
        self.assertEqual(out,self.file)
    
    def test_readf(self):
        
        out = iou.readf(self.txt_file)
        with open(self.txt_file) as infile:
            content = infile.read()
        
        self.assertMultiLineEqual(out,content)
        
    def test_load_json(self):
        
        out = iou.load_json(self.json_file)
        self.assertDictEqual(out,self.test_dict)        
    
    def test_load_pickle(self):
        out = iou.load_pickle(self.pkl_file)
        self.assertDictEqual(out,self.test_dict)
        
        
        
        
        

if __name__ == '__main__':
    unittest.main()


